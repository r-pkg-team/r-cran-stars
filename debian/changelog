r-cran-stars (0.6-8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Thu, 20 Feb 2025 08:45:50 +0900

r-cran-stars (0.6-7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 23 Jan 2025 11:07:37 +0900

r-cran-stars (0.6-6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Tue, 23 Jul 2024 13:45:43 +0900

r-cran-stars (0.6-5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Tue, 04 Jun 2024 08:26:37 +0900

r-cran-stars (0.6-4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 17 Oct 2023 15:16:03 +0200

r-cran-stars (0.6-3-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 28 Aug 2023 14:04:15 +0200

r-cran-stars (0.6-2-1) unstable; urgency=medium

  * New upstream version
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 21 Jul 2023 18:05:28 +0200

r-cran-stars (0.6-1-1) unstable; urgency=medium

  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)
  * Sync Test-Depends with Suggests
  * Rebuild with r-graphics-engine set by r-base
    Closes: #1039663
  * Since package exactextractr is not available yet (but in new)
    one test (and its output) needs to be patched

 -- Andreas Tille <tille@debian.org>  Thu, 06 Jul 2023 13:53:33 +0200

r-cran-stars (0.6-0-4) unstable; urgency=medium

  * Fix syntax in autopkgtest-pkg-r.conf (Thanks for the hint to
    Antonio Terceiro)

 -- Andreas Tille <tille@debian.org>  Thu, 26 Jan 2023 22:25:37 +0100

r-cran-stars (0.6-0-3) unstable; urgency=medium

  * Add autopkgtest-pkg-r.conf to skip tests on s390x also in
    autopkgtest-pkg-r
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 26 Jan 2023 11:41:45 +0100

r-cran-stars (0.6-0-2) unstable; urgency=medium

  * Do not run debci test for s390x since Test-Depends r-cran-lwgeom
    is not available here

 -- Andreas Tille <tille@debian.org>  Tue, 10 Jan 2023 21:27:02 +0100

r-cran-stars (0.6-0-1) unstable; urgency=medium

  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Mon, 28 Nov 2022 16:02:55 +0100

r-cran-stars (0.5-6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.5-6
  * dh-update-R: update versioned B-D
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 23 Jul 2022 20:00:28 +0530

r-cran-stars (0.5-5-2) unstable; urgency=medium

  * Team Upload.
  * Fix sf version in DESCRIPTION file so as to
    pull relevant debian version

 -- Nilesh Patra <nilesh@debian.org>  Tue, 04 Jan 2022 01:31:33 +0530

r-cran-stars (0.5-5-1) unstable; urgency=medium

  * New upstream version 0.5-5

 -- Nilesh Patra <nilesh@debian.org>  Fri, 31 Dec 2021 16:05:14 +0530

r-cran-stars (0.5-4-1) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * Test-Depends: r-cran-terra

  [ Nilesh Patra ]
  * New upstream version 0.5-4
  * d/control: Run dh-update-R

 -- Nilesh Patra <nilesh@debian.org>  Tue, 23 Nov 2021 19:26:23 +0530

r-cran-stars (0.5-3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Test-Depends: r-cran-spatstat.geom, versioned Test-Depends for
    r-cran-spatstat

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 15:16:28 +0200

r-cran-stars (0.5-1-2) unstable; urgency=high

  * Add skip-not-installable to autopkgtest restrictions to deal with
      E: Package 'r-cran-s2' has no installation candidate
    on armhf and i386

 -- Andreas Tille <tille@debian.org>  Thu, 11 Feb 2021 14:56:50 +0100

r-cran-stars (0.5-1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 31 Jan 2021 15:54:40 +0100

r-cran-stars (0.5-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * Test-Depends: r-cran-rgdal

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jan 2021 21:32:54 +0100

r-cran-stars (0.4-3-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Test-Depends: r-cran-cubelyr

 -- Andreas Tille <tille@debian.org>  Sat, 17 Oct 2020 23:31:10 +0200

r-cran-stars (0.4-1-3) unstable; urgency=medium

  * Source upload in R 4.0 transition

 -- Andreas Tille <tille@debian.org>  Tue, 19 May 2020 13:39:33 +0200

r-cran-stars (0.4-1-2) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest

 -- Dylan Aïssi <daissi@debian.org>  Mon, 13 Apr 2020 16:10:15 +0200

r-cran-stars (0.4-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 08 Apr 2020 09:07:13 +0200

r-cran-stars (0.4-0-2) unstable; urgency=medium

  * r-cran-ncdfgeom available include test
  * Set upstream metadata fields: Bug-Submit, Repository.

 -- Andreas Tille <tille@debian.org>  Wed, 25 Dec 2019 23:18:08 +0100

r-cran-stars (0.4-0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database, Repository.
  * Exclude test requiring r-cran-ncdfgeom
  * Better long description

 -- Andreas Tille <tille@debian.org>  Thu, 14 Nov 2019 17:07:44 +0100

r-cran-stars (0.3-1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * rename debian/tests/control.autodep8 to debian/tests/control
  * Test-Depends: r-cran-spacetime, r-cran-pcict (just uploaded to new so
    test will fail until these and the dependencies are accepted)
  * Test-Depends: r-cran-ncmeta, r-cran-raster, r-cran-ggplot2, r-cran-lwgeom

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jul 2019 10:42:49 +0200

r-cran-stars (0.2-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 26 Nov 2018 08:58:42 +0100

r-cran-stars (0.1-1-1) unstable; urgency=medium

  * Initial release (closes: #907027)

 -- Andreas Tille <tille@debian.org>  Thu, 23 Aug 2018 10:58:56 +0200
